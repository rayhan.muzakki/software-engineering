package com.example.demo.Core;

public class Customer {
    private int sanitizerInput;
    private int maskerInput;
    private int donationInput;
    private int totalCost;
    private String email;
    private String address;

    public int getSanitizerInput() {
        return sanitizerInput;
    }

    public void setSanitizerInput(int sanitizerInput) {
        this.sanitizerInput = sanitizerInput;
    }

    public int getMaskerInput() {
        return maskerInput;
    }

    public void setMaskerInput(int maskerInput) {
        this.maskerInput = maskerInput;
    }

    public int getDonationInput() {
        return donationInput;
    }

    public void setDonationInput(int donationInput) {
        this.donationInput = donationInput;
    }

    public int getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(int totalCost) {
        this.totalCost = totalCost;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
