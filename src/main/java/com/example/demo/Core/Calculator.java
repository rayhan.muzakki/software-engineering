package com.example.demo.Core;

public class Calculator {
    private int sanitizerCost= 7000;
    private static int testInput1=4;
    private int maskerCost = 15000;
    private static int testInput2=1;
    private static int testDonationInput=1000;

    public int calculate(int inputSan,int inputMask,int donation){
        int totalfeeSanitizer= inputSan*sanitizerCost;
        int totalfeeMasker = inputMask*maskerCost;

        int totalfee= totalfeeMasker+totalfeeSanitizer+donation;
        return totalfee;
    }

    public static void main(String[] args){
        Calculator calculator= new Calculator();
        int result= calculator.calculate(testInput1,testInput2,testDonationInput);
        System.out.println(result);
    }

}
