package com.example.demo.Controller;

import com.example.demo.Core.Calculator;
import com.example.demo.Core.Customer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MainController {
    int result;
    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("customer", new Customer());
        return "index";
    }

    @PostMapping("/")
    public String homeSubmit(@ModelAttribute Customer customer){
        Calculator calculator= new Calculator();
        result=calculator.calculate(customer.getSanitizerInput(),customer.getMaskerInput(),customer.getDonationInput());
        customer.setTotalCost(result);
        System.out.print(customer.getTotalCost());
        return "result";
    }
}
